<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;


class CustomerController extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'bedrijf' => 'required',
                'contactpersoon' => 'required',
                'email' => 'required|email',
                'pakket' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data_array =  array(
            'bedrijf' => $request->input('bedrijf'),
            'contactpersoon' => $request->input('contactpersoon'),
            'email' => $request->input('email'),
            'pakket' => $request->input('pakket'),
            'partner' => Auth::id(),
        );

        // var_dump($request->input('partner'));
        // exit();

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://app.pricesearch.nl/api/signupbypartner",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data_array
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($response);

        if($result->success){
            return redirect('/klanten');
        } else {
            return redirect('/');
        }

    }

    public function getUsers(Request $request)
    {
        // $partner = 1;
        $partner = Auth::id();

        $validator = Validator::make($request->all(),
            [
                // 'partner' => 'required',
            ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://app.pricesearch.nl/api/usersbypartner",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('partner' => $partner /*$request->input('partner')*/),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;

    }

}
