'use strict';
var KTDatatablesDataSourceAjaxClient = function(csrfToken) {

	var initTable1 = function(csrfToken) {
		var table = $('#kt_table_1');

		// begin first table
		table.DataTable({
			responsive: true,
			ajax: {
				url: 'http://127.0.0.1:8000/klantenData',
				type: 'POST',

                headers: {
                    'X-CSRF-TOKEN': csrfToken,
                },
				data: {
					pagination: {
						perpage: 50,
					},
				},
			},
			columns: [
                {data: 'user_id'},
				{data: 'firstname'},
				{data: 'lastname'},
				{data: 'company'},
				{data: 'country'},
                {data: 'user_id'},

            ],
			columnDefs: [
				{
					targets: -1,
					title: 'Acties',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <a onclick="document.getElementById('form_id_` + data + `').submit();" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                          <i class="la la-edit"></i>
                        </a>
                        
                        <form action='modifyKlanten' method='post' id="form_id_` + data + `">
                            <input type="hidden" name="_token" id="csrf-token" value="`+ csrfToken + `" />
                            <input type="hidden" name="user_id" value="` + data + `">
                        </form>`;

					},
				},
				{
					targets: -3,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Pending', 'class': 'kt-badge--brand'},
							2: {'title': 'Delivered', 'class': ' kt-badge--danger'},
							3: {'title': 'Canceled', 'class': ' kt-badge--primary'},
							4: {'title': 'Success', 'class': ' kt-badge--success'},
							5: {'title': 'Info', 'class': ' kt-badge--info'},
							6: {'title': 'Danger', 'class': ' kt-badge--danger'},
							7: {'title': 'Warning', 'class': ' kt-badge--warning'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
					},
				},
				{
					targets: -2,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Online', 'state': 'danger'},
							2: {'title': 'Retail', 'state': 'primary'},
							3: {'title': 'Direct', 'state': 'success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
							'<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
			],
		});
	};

	return {

		//main function to initiate the module
		init: function(csrfToken) {
			initTable1(csrfToken);
		},

	};

}();
