@extends('layouts.app')
@section('title', 'customer')
@section('content')
<div class="kt-portlet kt-portlet--head-lg kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Nieuwe klant aanmaken
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <form class="column justify-content-center orm-group col-5" method="POST" action="{{ route('signupbypartner') }}">
            @csrf
            <div class="form-group row">
                <label class="col-3 col-form-label">Bedrijf:</label>
                <input class="form-control col-9" required name="bedrijf" type="text" placeholder="Bedrijf" />
            </div>
            <div class="form-group row">
                <label class="col-3 col-form-label">Contactpersoon:</label>
                <input class="form-control col-9" required name="contactpersoon" type="text" placeholder="Contactpersoon" />
            </div>
            <div class="form-group row">
                <label class="col-3 col-form-label">Email:</label>
                <input class="form-control col-9" required name="email" type="email" placeholder="Email" />
            </div>
            <div class="form-group row">
                <label class="col-3 col-form-label">Pakket:</label>
                <select name="pakket" class="custom-select form-control col-9">
                    <option value="1">Kies een pakket</option>
                    <option value="1">Starter</option>
                    <option value="2">Standard</option>
                    <option value="3">Professional</option>
                    <option value="4">Plus</option>
                </select>
            </div>
            <div class="justify-content-end row">
                <button type="submit" class="btn btn-success mr-2">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </form>
    </div>
</div>

@endsection
