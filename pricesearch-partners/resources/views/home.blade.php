@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<div class="kt-portlet kt-portlet--head-lg kt-portlet--mobile">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Dashboard
            </h3>
        </div>
    </div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            Welcome
            <?php
                $user = $user = Auth::user();
                echo($user->name);
            ?>
            !
    </div>
</div>
@endsection
