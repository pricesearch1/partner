@extends('layouts.app')
@section('title', 'Klanten')
@section('content')
    <div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="kt-portlet kt-portlet--head-lg kt-portlet--mobile">
                        <div class="kt-portlet__head d-flex justify-content-between align-items-center">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Klanten
                                </h3>
                            </div>
                            <div class="kt-notification__custom">
                                <a href="/customer" class="btn btn-success">Create</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="kt-section">
                            <div class="kt-section__content">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Company</th>
                                            <th>Pakket</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table1">

                                    </tbody>
                                </table>

                                <script>
                                    function loadTable(){
                                        fetch('http://localhost:8000/v1/usersbypartner', {
                                            method: 'GET',
                                        })
                                            .then(function(resp) {
                                                return resp.json();
                                            })
                                            .then(function(data) {
                                                var json = data.users;
                                                var test = '';
                                                var table = document.getElementById("table1");

                                                for(var i = 0; i < (json.length); i++){
                                                    var obj = json[i];
                                                    console.log(obj['groupId'])
                                                    switch(obj['groupId']){
                                                        case '1':
                                                            test = 'Starter';
                                                            break;
                                                        case '2':
                                                            test = 'Standard';
                                                            break;
                                                        case '3':
                                                            test = 'Professional';
                                                            break;
                                                        case '4':
                                                            test = 'Plus';
                                                            break;
                                                    }

                                                    console.log(test);

                                                    var tray = table.insertRow(obj);
                                                    var cell1 = tray.insertCell(0);
                                                    var cell2 = tray.insertCell(1);
                                                    var cell3 = tray.insertCell(2);
                                                    var cell4 = tray.insertCell(3);
                                                    var cell5 = tray.insertCell(4);

                                                    cell1.innerHTML = obj['id'];
                                                    cell2.innerHTML = obj['username'];
                                                    cell3.innerHTML = obj['email'];
                                                    cell4.innerHTML = obj['company'];
                                                    cell5.innerHTML = test
                                                }
                                            })
                                        }
                                        window.addEventListener('load', (event) => {
                                            loadTable();
                                        });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
