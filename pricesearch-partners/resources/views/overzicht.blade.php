@extends('layouts.app')
@section('title', 'Overview')
@section('content')
    <div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                <div class="kt-portlet kt-portlet--head-lg kt-portlet--mobile">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Overview
                            </h3>
                        </div>
                    </div>
                </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="kt-section">
                            <div class="kt-section__content">
                                <table id="overzichtTable" class="table table-hover">

                                    <thead>
                                        <tr>
                                            <th>Number</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <?php

                                        foreach( $facturen as $overzicht ) {

                                            $href = 'overview/' . $overzicht->factuur_id;
                                            $date = date('m-d-Y', strtotime($overzicht->datum));

                                            print("<tr class='clickable-row' data-href='$href'>");
                                            print("<th scope='row'> $overzicht->factuur_id </th>");
                                            print("<td> $date </td>");
                                            print("<td>€ $overzicht->bedrag </td>");

                                            // 0 = wachtende
                                            // 1 = uitbetaald
                                            if($overzicht->status)
                                                print("<td class='bg-success'>Paid</td>");
                                            else
                                                print("<td class='bg-warning'>Waiting</td>");

                                            print("</tr>");
                                        }

                                    ?>
                                    </tbody>

                                </table>
                                <script>
                                    // makes the invoice table sortable
                                    $(document).ready(function () {
                                        $('#overzichtTable').DataTable({
                                            'paging': false,
                                            'searching': false,
                                            'bInfo': false
                                        });
                                        $('.dataTables_length').addClass('bs-select');
                                    });

                                    // makes a row in the invoice table clickable
                                    jQuery(document).ready(function($) {
                                        $(".clickable-row").click(function() {
                                            window.location = $(this).data("href");
                                        });
                                    });
                                </script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
