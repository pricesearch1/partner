@extends('layouts.app')
@section('title', 'Reset pagina')
@section('content')
<div>
    <?php
        if(isset($passwordReset)){

            echo("
                <script>
                    $( document ).ready(function() {
                        $('#exampleModal').modal('show');
                    });
                </script>
            ");

        }
    ?>
    <?php
        if(isset($passwordReset)){
            if($passwordReset){
                echo("<div class='alert alert-success'> <strong>Your password has succesfully been reset! </strong> </div>");
            }else{
                echo("<div class='alert alert-warning'>Your Password could not be changed.</div> Did you enter the password twice correctly?");
            }
        }
    ?>
    <div class="kt-portlet kt-portlet--head-lg kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Reset Password
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form class="column justify-content-center orm-group col-5" method="POST" action="{{ route('resetPassword') }}">
                @csrf
                <div class="form-group row">
                    <label for="password" class="col-3 col-form-label">{{ __('Password') }}</label>
                    <input id="password" class="form-control col-9 @error('password') is-invalid @enderror" type="password" name="password" required autocomplete="new-password"/>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <label for="password-confirm" class="col-3 col-form-label">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" class="form-control col-9" type="password" name="password_confirmation" required autocomplete="new-password" />
                </div>
                <div class="justify-content-end row">
                    <button type="submit" class="btn btn-success mr-2">{{ __('Reset Password') }}</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
