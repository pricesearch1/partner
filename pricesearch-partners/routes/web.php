<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/reset', 'ResetController@index')->name('reset');

Route::post('/reset', 'ResetController@updatePassword')->name('resetPassword');

Route::get('/customer', 'CustomerController@index')->name('customer');

Route::post('/customer', 'CustomerController@createCustomer')->name('createCustomer');

Route::get('/overview', 'HomeController@overzicht')->name('overzicht');

Route::get('/overview/{factuur_id}', ['uses' => 'HomeController@showExtraOverzicht']);

Route::get('/logout', 'Auth\LoginController@logout');

Route::post('fileUpload', [
    'as' => 'image.add',
    'uses' => 'HomeController@uploadContract'
]);

Route::get('/klanten', function() {
    return view('klanten');
});

Route::prefix('v1')->group(function(){
    Route::get('usersbypartner', 'Api\CustomerController@getUsers');
    Route::post('signupbypartner', 'Api\CustomerController@register')->name('signupbypartner');
});
